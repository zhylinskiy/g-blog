<?php

namespace App\Http\Requests;

class RegisterRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'email' => 'email|required|unique:users,email',
            'name' => 'string|required|min:2|max:64',
            'password' => 'string|required|min:6|max:24|confirmed'
        ];
    }
}
