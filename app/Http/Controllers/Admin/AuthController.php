<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LoginRequest;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        $attempt = Auth::guard('web')->attempt($credentials);
        if ($attempt) {
            $request->session()->regenerate();
            
            return redirect()->intended();
        }
        
        return view('admin.error.500'); 
    }
}