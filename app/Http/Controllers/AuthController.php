<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\Role;
use App\Models\User;
use App\Traits\ApiResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    use ApiResponse;

    public function register(RegisterRequest $request)
    {
        $data = $request->only('email', 'name');

        $user = new User($data);
        $user->password = bcrypt($request->get('password'));

        try {
            $user->save();

            $role = Role::where('slug', 'author')->first();
            $user->roles()->attach($role);

            return $this->successResponse();
        } catch (\Throwable $e) {
            return $this->failResponse($e->getMessage());
        }
    }

    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            $token = JWTAuth::attempt($credentials);

            if (!$token) {
                return $this->errorResponse('Invalid email or password', [], 401);
            }

            return $this->successResponse(['token' => $token]);
        } catch (\Throwable $e) {
            return $this->failResponse($e->getMessage());
        }
    }

    public function logout()
    {
        JWTAuth::invalidate(JWTAuth::getToken());
    }

    public function changePassword(Request $request)
    {
        $id = Auth::id();
        $pass = $request->get('password');
        $user = User::where('id', $id)->first();

        if (Hash::check($pass, $user->password)) {

            $newPassword = bcrypt($request->get('newPassword'));
            $user->password = $newPassword;

            if (!$user->save()) {
                return $this->failResponse('Some trouble with save');
            }
            JWTAuth::invalidate(JWTAuth::getToken());

            return $this->successResponse();
        }
        return $this->errorResponse('Wrong current password');
    }
}