<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\JWT;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    use ApiResponse;

    public function index()
    {
        $posts = Post::all();

        return $this->successResponse($posts);
    }

    public function store(Request $request)
    {
        $title = $request->get('title');
        $body = $request->get('body');

        $userId = Auth::id();

        $post = Post::create([
            'title' => $title,
            'body' => $body,
            'userId' => $userId
        ]);

        if (!$post) {
            return $this->failResponse('Failed to create');
        }

        return $this->successResponse();
    }

    public function show($id)
    {
        $post = Post::where('id', $id)->first();
        if (!$post ) {
            return $this->errorResponse('Invalid post ID');
        }

        return $this->successResponse($post);
    }

    public function update($id, Request $request)
    {
        $post = Post::where('id', $id)->first();

        $post->fill($request->all());

        if (!$post->save()) {
            return $this->failResponse("Failed to update");
        }

        return $this->successResponse();
    }

    public function destroy($id)
    {
        $post = Post::where('id', $id)->first();

        if (!$post->delete()) {
            return $this->errorResponse('Failed to delete');
        }

        return $this->successResponse();
    }
}
