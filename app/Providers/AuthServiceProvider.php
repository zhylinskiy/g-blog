<?php

namespace App\Providers;

use App\Models\Post;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->registerPostPolicies();
    }

    protected function registerPostPolicies()
    {
        Gate::define('create.post', function ($user) {
            return $user->hasRole('author');
        });

        Gate::define('post.update', function ($user, $postId) {
            return ($user->hasAccess(['update-post']) && $user->id === $postId);
        });
    }
}
