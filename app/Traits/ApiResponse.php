<?php

namespace App\Traits;

trait ApiResponse
{
    /**
     * Successful response
     *
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function successResponse($data = [])
    {
        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Bad request
     *
     * @param $message
     * @param array $errors
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    protected function errorResponse($message, $errors = [], $status = 400)
    {
        return response()->json([
            'success' => false,
            'message' => $message,
            'errors' => $errors
        ], $status);
    }

    /**
     * Server error
     *
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function failResponse($message = 'Server failure')
    {
        return response()->json([
            'success' => false,
            'message' => $message
        ], 500);
    }
}