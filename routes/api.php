<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('logout', 'AuthController@logout');

    Route::post('change-password', 'AuthController@changePassword')->middleware('jwt.auth');

    Route::get('check', function () {
        return response()->json(['success' => true]);
    })->middleware('jwt.auth');
});

Route::prefix('post')->name('post.')->middleware('jwt.auth')->group(function () {
    Route::get('/', 'PostController@index')->name('index');
    Route::post('/', 'PostController@store')->name('create');
    Route::get('/{id}', 'PostController@show')->name('show');
    Route::put('/{id}', 'PostController@update')->middleware('can:post.update,id')->name('update');
    Route::delete('/{id}', 'PostController@destroy')->name('destroy');
});